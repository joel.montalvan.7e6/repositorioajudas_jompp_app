# JOMPP_RaulMolina_JoelMontalvan

## Descripción del Proyecto

JOMPP es una aplicación creado por Raul Molina y Joel Montalvan, estudiantes del Instituto Tecnológico de Barcelona (ITB). De hecho, el nombre del juego es una combinación de palabras de "JUEGOS ONLINE de M**** PARA PC", al que se le puso dicho nombre debido a la cantidad de juegos de internet que jugaban los creadores para entretenerse.

JOMPP es una app dedicada a guardar una cantidad de juegos de internet que el propio usuario introduce con la finalidad de no poder olvidarte de su nombre y página web destacada.

Su mecánica es muy simple, y posiblemente a esto se debe parte de su éxito, ya que hace que sea accesible para todos. Cada vez que guardas un juego, el usuario es capaz de cambiarlo a diferentes listas dependiendo del estado que se situa el juego para el usuario, ya sea como "Para jugar", "Favoritos", "Jugados", "Página Web del juego".
Además, puedes añadir y eliminar tantos juegos como quieras, incluso puedes ponerle una nota para valorarlo.

## Como instalar y ejecutar el proyecto

Descarga el proyecto en formato ZIP para después exportalo donde te vaya mejor en tu sistema operativo, a la vez, debes tener instalado un programa para poder ejecutar el proyecto, por ejemplo Intellij IDEA.

Una vez descargado el proyecto, debes abrirlo en el programa que tengas en tu sistema operativo. Y deberas esperar unos segundos a que se ejecute el proyecto con el juego.

Una vez instalado, ves hacia el boton del play y ya podras disfrutar de la aplicación. A JUGAR!!!

## Instrucciones del uso de la aplicación

Cuando inicies la app, lo primero que verás será la pantalla de login. Es sencillo, si no dispones de una cuenta te registras con tus datos, sino inicias normal, además dsispones de un boton switch para guardar tus datos y así no tener que escribir tus datos cada vez que entres.

Si no dispones de juegos añadidos, pulsa el botón (+) con el que introduciras todos los datos del juego para finalmente guardarlo.

Una vez guardado el juego, la aplicación puede distribuirlo de diferentes formas:

- JUEGO GUARDADO/ NO FAVORITO/ NO JUGADO -> Solo estara disponible en el apartado de JUGAR
- JUEGO GUARDADO/ FAVORITO/ NO JUGADO -> Solo estara disponible en el apartado de JUGAR y FAVORITO
- JUEGO GUARDADO/ FAVORITO/ JUGADO -> Solo estara disponible en el apartado de JUGADO y FAVORITO
- JUEGO GUARDADO -> Estara disponible en el apartado de su respectiva página WEB

## License

MIT License

Copyright (c) [2023] raul[itb]molina kind joel[itb]montalvan montiel 

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

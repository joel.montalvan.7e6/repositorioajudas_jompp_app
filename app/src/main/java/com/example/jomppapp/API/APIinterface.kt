package com.example.jomppapp.API

import com.burgstaller.okhttp.digest.Credentials
import com.burgstaller.okhttp.digest.DigestAuthenticator
import com.example.jomppapp.model.Game
import com.example.jomppapp.model.Usuarios
import com.google.gson.GsonBuilder
import okhttp3.MultipartBody
import okhttp3.OkHttpClient
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import java.io.File

interface APIinterface {

    @GET()
    suspend fun getFotos(@Url url: String): Response<ResponseBody>
    @GET()
    //LISTA DE JUEGOS PARA JUGAR PARA UN USUARIO
    suspend fun getJuegosNoJugados(@Url url: String): Response<List<Game>>
    @GET()
    //LISTA DE JUEGOS JUGADOS
    suspend fun getJuegosJugados(@Url url: String): Response<List<Game>>
    @GET()
    //LISTA DE JUEGOS FAVORITOS
    suspend fun getJuegosFavoritos(@Url url: String): Response<List<Game>>
    @GET()
    //DETALLE JUEGO
    suspend fun getJuego(@Url url: String): Response<Game>
    @GET()
    //LOG IN DE USUARIO
    suspend fun getLogin(@Url url: String): Response<Boolean>

    @DELETE()
    //BORRAR JUEGO POR ID
    suspend fun deleteGame(@Url url: String): Response<ResponseBody>

    @Multipart
    @POST("games")
    //AÑADIR JUEGO A LA LISTA DE JUEGOS PARA JUGAR
    suspend fun addGameJuegosNoJugados(
        @Part ("id") id: RequestBody,
        @Part ("nameUsuario") nameUsuario: RequestBody,
        @Part ("name") name: RequestBody,
        @Part ("genero") genero: RequestBody,
        @Part ("multiplayer") multiplayer: RequestBody,
        @Part ("pagina") pagina: RequestBody,
        @Part ("puntuacion") puntuacion: RequestBody,
        @Part ("favorito") favorito: RequestBody,
        @Part ("jugado") jugado: RequestBody,
        @Part image: MultipartBody.Part
    ): Response<Boolean>

    //REGISTRAR USUARIO
    @POST("usuarios")
    suspend fun registerUser(@Body requestBody: Usuarios): Response<Boolean>

    @PUT()
    //CAMBIAR JUEGO A JUEGO JUGADO
    suspend fun gameToPlayedGames(@Url url: String): Response<ResponseBody>
    @PUT()
    //AÑADIR JUEGO A FAVORITOS
    suspend fun gameToFavourites(@Url url: String): Response<ResponseBody>
    @PUT()
    //QUITAR JUEGO DE FAVORITOS
    suspend fun gameToNoFavourites(@Url url: String): Response<ResponseBody>
    @PUT()
    //PONER NOTA A JUEGO
    suspend fun puntuarJuego(@Url url: String): Response<ResponseBody>

    companion object {
        val BASE_URL = "http://169.254.19.23:8080/"
        fun create(username: String?, password: String?): APIinterface {
            val digestAuthenticator = DigestAuthenticator(Credentials(username, password))
            val gson = GsonBuilder().setLenient().create()

            val client = OkHttpClient.Builder()
                .authenticator(digestAuthenticator)
                .build()

            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build()
            return retrofit.create(APIinterface::class.java)
        }
    }
}
package com.example.jomppapp.Adapter

import com.example.jomppapp.model.PaginaDeJuegos

interface PageOnClickListener {
    fun onClick (page: PaginaDeJuegos)
}
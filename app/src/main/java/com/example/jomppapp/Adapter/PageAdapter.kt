package com.example.jomppapp.Adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.jomppapp.R
import com.example.jomppapp.databinding.ItemGameBinding
import com.example.jomppapp.databinding.ItemPageBinding
import com.example.jomppapp.model.Game
import com.example.jomppapp.model.PaginaDeJuegos
import java.io.File

class PageAdapter(private val paginas: List<PaginaDeJuegos>, private var listener: PageOnClickListener): RecyclerView.Adapter<PageAdapter.ViewHolder>() {

    inner class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        val binding = ItemPageBinding.bind(view)
        fun setListener(page: PaginaDeJuegos){
            binding.root.setOnClickListener {
                listener.onClick(page)
            }
        }
    }

    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.item_page, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return paginas.size
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val page = paginas[position]
        with(holder){
            setListener(page)
            binding.nombre.text = page.name
            binding.foto.setImageResource(page.logoPagina)
        }
    }


}
package com.example.jomppapp.Adapter

import com.example.jomppapp.model.Game

interface MyOnClickListener {
    fun onClick (game: Game)
}
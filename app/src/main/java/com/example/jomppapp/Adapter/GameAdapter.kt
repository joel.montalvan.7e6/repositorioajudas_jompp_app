package com.example.jomppapp.Adapter

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.BitmapFactory
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.jomppapp.API.APIinterface
import com.example.jomppapp.API.Repository
import com.example.jomppapp.R
import com.example.jomppapp.databinding.ItemGameBinding
import com.example.jomppapp.model.Game
import com.example.jomppapp.viewModel.GameViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class GameAdapter(private val games: List<Game>, private var listener: MyOnClickListener): RecyclerView.Adapter<GameAdapter.ViewHolder>() {
    var repository= Repository("admin", "password")
    inner class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        val binding = ItemGameBinding.bind(view)
        fun setListener(game: Game){
            binding.root.setOnClickListener {
                listener.onClick(game)
            }
        }
    }

    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.item_game, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return games.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val game = games[position]
        with(holder){
            setListener(game)
            binding.nombre.text = game.name
            binding.nota.text = "${game.puntuacion.toString()} ⭐"
            CoroutineScope(Dispatchers.IO).launch {
                val response = repository.getFotos(game.id)
                withContext(Dispatchers.Main){
                    if(response.isSuccessful && response.body() != null){
                        val foto = response.body()!!.bytes()
                        Glide.with(context)
                            .load(foto)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .centerCrop()
                            .circleCrop()
                            .into(binding.foto)
                    }
                }
            }
        }
    }


}

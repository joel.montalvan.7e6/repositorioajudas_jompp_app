package com.example.jomppapp.viewModel

import android.content.ContentValues.TAG
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.jomppapp.API.Repository
import com.example.jomppapp.model.Game
import com.example.jomppapp.model.Usuarios
import kotlinx.coroutines.*
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import java.io.File

class GameViewModel: ViewModel() {
    var data = MutableLiveData<List<Game>>()
    val userName = MutableLiveData<String>()
    val password = MutableLiveData<String>()

    //fun fetchData(){
    //        CoroutineScope(Dispatchers.IO).launch {
    //            val response = Repository().getJuegosNoJugados(userName.value)
    //            withContext(Dispatchers.Main) {
    //                if(response.isSuccessful){
    //                    data.postValue(Repository().getJuegosNoJugados(userName.value).body())
    //                    println(data.value)
    //                }
    //            }
    //        }
    //    }

    fun getJuegosNoJugados(){
        CoroutineScope(Dispatchers.IO).launch {
            val response = Repository(userName.value, password.value).getJuegosNoJugados(userName.value)
            withContext(Dispatchers.Main){
                if(response.isSuccessful){
                    data.postValue(Repository(userName.value, password.value).getJuegosNoJugados(userName.value).body())
                }
            }
        }
    }
    fun getJuegosJugados(){
        CoroutineScope(Dispatchers.IO).launch {
            data.postValue(Repository(userName.value, password.value).getJuegosJugados(userName.value).body())
        }
    }
    fun getJuegosFavoritos(){
        CoroutineScope(Dispatchers.IO).launch {
            data.postValue(Repository(userName.value, password.value).getJuegosFavoritos(userName.value).body())
        }
    }
    fun addJuego(juego: Game, image: File){
        CoroutineScope(Dispatchers.IO).launch {
            val imagePart = MultipartBody.Part.createFormData("jpeg", image.name, image.asRequestBody("image/*".toMediaType()))
            val resposta = Repository(userName.value, password.value).addGameJuegosNoJugados(
                juego.id.toRequestBody("text/plain".toMediaType()),
                juego.nameUsuario.toRequestBody("text/plain".toMediaType()),
                juego.name.toRequestBody("text/plain".toMediaType()),
                juego.genero.toRequestBody("text/plain".toMediaType()),
                juego.multiplayer.toString().toRequestBody("text/plain".toMediaType()),
                juego.pagina.toRequestBody("text/plain".toMediaType()),
                juego.puntuacion.toString().toRequestBody("text/plain".toMediaType()),
                juego.favorito.toString().toRequestBody("text/plain".toMediaType()),
                juego.jugado.toString().toRequestBody("text/plain".toMediaType()),
                imagePart
            )
            withContext(Dispatchers.Main){
                if(resposta.isSuccessful){
                    Log.d(TAG, "Juego añadido")
                }else{
                    Log.d(TAG, "Ha habido un error")
                }
            }

        }

    }
    fun juegoToPlayed(id: String){
        var procede = true
        CoroutineScope(Dispatchers.IO).launch {
            Repository(userName.value, password.value).gameToPlayedGames(id)
            procede=false
        }
        while (procede){

        }
    }
    fun puntuarGame(id:String, nota: String){
        var procede = true
        CoroutineScope(Dispatchers.IO).launch {
            Repository(userName.value, password.value).puntuarJuego(id, nota)
            procede=false
        }
        while (procede){

        }
    }
    fun deleteJuego(id: String){
        var procede = true
        CoroutineScope(Dispatchers.IO).launch {
            Repository(userName.value, password.value).deleteGame(id)
            procede=false
        }
        while (procede){

        }
    }
    fun favouriteGame(id: String){
        var procede = true
        CoroutineScope(Dispatchers.IO).launch {
            Repository(userName.value, password.value).gameToFavourites(id)
            procede=false
        }
        while (procede){

        }
    }
    fun noFavouriteGame(id: String){
        var procede = true
        CoroutineScope(Dispatchers.IO).launch {
            Repository(userName.value, password.value).gameToNoFavourites(id)
            procede=false
        }
        while (procede){

        }
    }

    //LOGIN Y REGISTER
    fun login(userName: String, password: String):Boolean{
        var retorno = false
        var procede = true
        CoroutineScope(Dispatchers.IO).launch {
            retorno = Repository(userName, password).getLogin().body() == true
            procede = false
        }
        while (procede){

        }
        return retorno
    }
    fun register(userName: String, password: String): Boolean{
        var retorno = false
        var procede = true
        val user = Usuarios(userName, password)
        CoroutineScope(Dispatchers.IO).launch {
            retorno = Repository("admin", "password").registerUser(user).body()!!
            procede=false
        }
        while (procede){

        }
        return retorno
    }
}
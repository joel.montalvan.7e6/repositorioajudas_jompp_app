package com.example.jomppapp.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.jomppapp.Adapter.GameAdapter
import com.example.jomppapp.Adapter.MyOnClickListener
import com.example.jomppapp.R
import com.example.jomppapp.databinding.FragmentListOfFilteredGamesByPageBinding
import com.example.jomppapp.databinding.FragmentListOfUserGamesBinding
import com.example.jomppapp.model.Game
import com.example.jomppapp.viewModel.GameViewModel

class ListOfFilteredGamesByPageFragment : Fragment(), MyOnClickListener {

    private lateinit var gameAdapter: GameAdapter
    private lateinit var linearLayoutManager: RecyclerView.LayoutManager
    private lateinit var binding: FragmentListOfFilteredGamesByPageBinding
    var pageName = ""
    lateinit var gameViewModel: GameViewModel
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        gameViewModel = ViewModelProvider(requireActivity())[GameViewModel::class.java]
        binding = FragmentListOfFilteredGamesByPageBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        gameViewModel.getJuegosJugados()

        pageName = arguments?.getString("pagina").toString()
        println(pageName)
        gameViewModel.data.observe(viewLifecycleOwner){ info ->
            setUpRecyclerView(info!! as MutableList<Game>)
        }
    }

    fun setUpRecyclerView(lista: MutableList<Game>){
        val useList = mutableListOf<Game>()
        for(i in lista){
            if(i.pagina == pageName){
                useList.add(i)
            }
        }

        gameAdapter = GameAdapter(useList, this)

        linearLayoutManager = LinearLayoutManager(context)

        binding.recyclerView.apply {
            setHasFixedSize(true)
            layoutManager = linearLayoutManager
            adapter = gameAdapter
        }

    }

    override fun onClick(game: Game) {
        val action = ListOfFilteredGamesByPageFragmentDirections.actionListOfFilteredGamesByPageFragmentToDetailFragment(
            game.name,
            game.pagina,
            game.multiplayer,
            game.favorito,
            game.puntuacion!!.toInt(),
            game.id,
            3,
            game.genero,
            pageName
        )
        findNavController().navigate(action)
    }

}
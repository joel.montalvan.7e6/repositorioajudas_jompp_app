package com.example.jomppapp.model

data class PaginaDeJuegos(
    var name: String,
    var logoPagina: Int //Ruta foto
)
